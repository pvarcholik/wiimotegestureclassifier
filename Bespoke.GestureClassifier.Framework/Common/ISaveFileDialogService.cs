﻿namespace Bespoke.Common
{
    public interface ISaveFileDialogService
    {
        string Title { get; set; }

        string Filter { get; set; }

        string FileName { get; }

        bool? ShowDialog();
    }
}
