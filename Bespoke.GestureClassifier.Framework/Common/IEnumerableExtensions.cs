﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bespoke.Common
{
    public static class IEnumerableExtensions
    {
        public static double Median<T>(this IEnumerable<T> source, Func<T, double> selector)
        {
            if (source.Count() == 0)
            {
                throw new InvalidOperationException("Cannot compute median for an empty set.");
            }

            return Median(source.Select(selector));
        }

        public static double Median(this IEnumerable<double> source)
        {
            if (source.Count() == 0)
            {
                throw new InvalidOperationException("Cannot compute median for an empty set.");
            }

            var sortedList = from number in source
                             orderby number
                             select number;

            int count = sortedList.Count();
            int index = count / 2;

            if (count % 2 == 0)
            {
                // Even number of items. 
                return (sortedList.ElementAt(index) + sortedList.ElementAt(index - 1)) / 2;
            }
            else
            {
                // Odd number of items. 
                return sortedList.ElementAt(index);
            }
        }
    }
}
