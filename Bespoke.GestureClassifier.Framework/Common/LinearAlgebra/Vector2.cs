using System;
using System.IO;

namespace Bespoke.Common.LinearAlgebra
{
    public struct Vector2
	{
		public Vector2(Point location)
			: this(location.X, location.Y)
		{
		}

		public Vector2(float x, float y)
		{
			X = x;
			Y = y;
		}

        #region Operators

        public static Vector2 operator -(Vector2 value) => new Vector2(-value.X, -value.Y);

        public static Vector2 operator +(Vector2 lhs, Vector2 rhs) => new Vector2(lhs.X + rhs.X, lhs.Y + rhs.Y);
        
        public static Vector2 operator -(Vector2 lhs, Vector2 rhs) => new Vector2(lhs.X - rhs.X, lhs.Y - rhs.Y);

        public static Vector2 operator *(float scaleFactor, Vector2 value) => new Vector2(value.X * scaleFactor, value.Y * scaleFactor);

        /// <summary>
        /// Compares a vector for equality with another vector.
        /// </summary>
        /// <param name="lhs">Source vector.</param>
        /// <param name="rhs">Source vector.</param>
        /// <returns>true if the vectors are equal; false otherwise.</returns>
        public static bool operator ==(Vector2 lhs, Vector2 rhs) => (lhs.X == rhs.X && lhs.Y == rhs.Y);

        /// <summary>
        /// Tests a vector for inequality with another vector.
        /// </summary>
        /// <param name="lhs">The vector on the left of the equal sign.</param>
        /// <param name="rhs">The vector on the right of the equal sign.</param>
        /// <returns>true if the vectors are not equal; false otherwise.</returns>
        public static bool operator !=(Vector2 lhs, Vector2 rhs) => !(lhs == rhs);

        #endregion

        public static float Dot(Vector2 lhs, Vector2 rhs) => ((lhs.X * rhs.X) + (lhs.Y * rhs.Y));

        public static float Distance(Vector2 lhs, Vector2 rhs)
		{
			float deltaX = lhs.X - rhs.X;
			float deltaY = lhs.Y - rhs.Y;
			float distanceSquared = (deltaX * deltaX) + (deltaY * deltaY);

			return (float)Math.Sqrt(distanceSquared);
		}

		public static Vector2 Normalize(Vector2 vector)
		{
			Vector2 normalizedVector;

			float inverseLength = 1.0f / vector.Length();
			normalizedVector.X = vector.X * inverseLength;
			normalizedVector.Y = vector.Y * inverseLength;
			
			return normalizedVector;
		}

        public static float GetAngle(Vector2 fromVector, Vector2 toVector)
        {
            fromVector.Normalize();
            toVector.Normalize();

            float angle = (float)Math.Acos(Vector2.Dot(fromVector, toVector));

            if (toVector.X - fromVector.X < 0.0f)
            {
                angle *= -1;
            }

            return angle;
        }

		public static Vector2 Load(BinaryReader reader)
		{
			Assert.ParamIsNotNull(nameof(reader), reader);

			float x = reader.ReadSingle();
			float y = reader.ReadSingle();

			return new Vector2(x, y);
		}

		public void Save(BinaryWriter writer)
		{
			Assert.ParamIsNotNull(nameof(writer), writer);

			writer.Write(X);
			writer.Write(Y);
		}

		/// <summary>
		/// Determines whether the specified System.Object is equal to the Vector.
		/// </summary>
		/// <param name="other">The System.Object to compare with the current Vector.</param>
		/// <returns>true if the specified System.Object is equal to the current Vector; false otherwise.</returns>
		public override bool Equals(object other)
		{
			if (!(other is Vector2))
			{
				return false;
			}

			return this == (Vector2)other;
		}

        /// <summary>
        /// Gets the hash code of this object.
        /// </summary>
        /// <returns>Hash code of this object.</returns>
        public override int GetHashCode() => base.GetHashCode();

        public void Normalize()
		{
			float inverseLength = 1.0f / Length();
			X *= inverseLength;
			Y *= inverseLength;
		}

        public float Length() => (float)Math.Sqrt((X * X) + (Y * Y));

        public Point ToPoint() => new Point((int)X, (int)Y);

        public static readonly Vector2 Zero = new Vector2(0.0f, 0.0f);

		public float X;
		public float Y;
	}
}
