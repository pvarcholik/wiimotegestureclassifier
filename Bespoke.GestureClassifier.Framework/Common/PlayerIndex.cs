﻿namespace Bespoke.Common
{
    public enum PlayerIndex
    {
        /// <summary>
        /// The first player.
        /// </summary>
        One = 0,

        /// <summary>
        /// The second player.
        /// </summary>
        Two = 1,

        /// <summary>
        /// The third player.
        /// </summary>
        Three = 2,

        /// <summary>
        /// The fourth player.
        /// </summary>
        Four = 3
    }
}
