﻿using System;
using System.Collections.Generic;
using System.IO;
using WiimoteLib;

namespace Bespoke.GestureClassifier.Framework
{
    public class WiimotePoint : IComparable<WiimotePoint>
    {
        #region Properties

        public DateTime TimeStamp { get; set; }

        public AccelState AccelState { get; set; }

        public ButtonState ButtonState { get; set; }

        #endregion

        public WiimotePoint(WiimoteState wiimoteState, DateTime timestamp)
        {
            TimeStamp = timestamp;
            AccelState = wiimoteState.AccelState;
            ButtonState = wiimoteState.ButtonState;            
        }

        public WiimotePoint(AccelState accelState, ButtonState buttonState, DateTime timeStamp)
        {
            TimeStamp = timeStamp;
            AccelState = accelState;
            ButtonState = buttonState;
        }

        #region Public Methods

        public static WiimotePoint Load(BinaryReader reader)
        {
            DateTime timeStamp = DateTime.FromBinary(reader.ReadInt64());

            AccelState accelState = new AccelState();
			accelState.RawValues.X = reader.ReadInt32();
			accelState.RawValues.Y = reader.ReadInt32();
			accelState.RawValues.Z = reader.ReadInt32();
            accelState.Values.X = reader.ReadSingle();
            accelState.Values.Y = reader.ReadSingle();
            accelState.Values.Z = reader.ReadSingle();

            ButtonState buttonState = new ButtonState();
            buttonState.A = reader.ReadBoolean();
            buttonState.B = reader.ReadBoolean();
            buttonState.Plus = reader.ReadBoolean();
            buttonState.Home = reader.ReadBoolean();
            buttonState.Minus = reader.ReadBoolean();
            buttonState.One = reader.ReadBoolean();
            buttonState.Two = reader.ReadBoolean();
            buttonState.Up = reader.ReadBoolean();
            buttonState.Down = reader.ReadBoolean();
            buttonState.Left = reader.ReadBoolean();
            buttonState.Right = reader.ReadBoolean();

            return new WiimotePoint(accelState, buttonState, timeStamp);
        }

        public void Save(BinaryWriter writer)
        {
            writer.Write(TimeStamp.ToBinary());
            
            writer.Write(AccelState.RawValues.X);
            writer.Write(AccelState.RawValues.Y);
            writer.Write(AccelState.RawValues.Z);
            writer.Write(AccelState.Values.X);
            writer.Write(AccelState.Values.Y);
            writer.Write(AccelState.Values.Z);

            writer.Write(ButtonState.A);
            writer.Write(ButtonState.B);
            writer.Write(ButtonState.Plus);
            writer.Write(ButtonState.Home);
            writer.Write(ButtonState.Minus);
            writer.Write(ButtonState.One);
            writer.Write(ButtonState.Two);
            writer.Write(ButtonState.Up);
            writer.Write(ButtonState.Down);
            writer.Write(ButtonState.Left);
            writer.Write(ButtonState.Right);
        }

        public int CompareTo(WiimotePoint other) => TimeStamp.CompareTo(other.TimeStamp);

        #endregion     
    }

    public class WiimotePointCollection : SortedList<DateTime, WiimotePoint>
    {
        public void Add(WiimotePoint wiimotePoint)
        {
            Add(wiimotePoint.TimeStamp, wiimotePoint);
        }

        public WiimotePoint[] ToArray()
        {
            WiimotePoint[] points = new WiimotePoint[Count];
            Values.CopyTo(points, 0);

            return points;
        }
    }
}
