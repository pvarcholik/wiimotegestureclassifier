﻿using System;

namespace Bespoke.GestureClassifier.Framework
{
    public class FeatureAttribute : Attribute
    {
        public Gesture.Features Feature { get; }

        public FeatureAttribute(Gesture.Features feature)
        {
            Feature = feature;
        }
    }
}
