﻿using Microsoft.Win32;
using Bespoke.Common;

namespace Bespoke.GestureClassifier.Wpf
{
    public class WpfSaveFileDialogService : ISaveFileDialogService
    {
        public string Title
        {
            get
            {
                return mSaveFileDialog.DefaultExt;
            }
            set
            {
                mSaveFileDialog.DefaultExt = value;
            }
        }

        public string Filter
        {
            get
            {
                return mSaveFileDialog.Filter;
            }
            set
            {
                mSaveFileDialog.Filter = value;
            }
        }

        public string FileName
        {
            get
            {
                return mSaveFileDialog.FileName;
            }
            private set
            {
                mSaveFileDialog.FileName = value;
            }
        }

        public WpfSaveFileDialogService()
        {
            mSaveFileDialog = new SaveFileDialog();
        }

        public bool? ShowDialog() => mSaveFileDialog.ShowDialog();

        private SaveFileDialog mSaveFileDialog;
    }
}
