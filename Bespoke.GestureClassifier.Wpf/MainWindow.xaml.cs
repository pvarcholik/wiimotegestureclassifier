﻿using System.Windows;
using Bespoke.GestureClassifier.Wpf.ViewModels;
using Microsoft.Practices.Unity;

namespace Bespoke.GestureClassifier.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [Dependency]
        public MainWindowViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
