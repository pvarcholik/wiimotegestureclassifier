﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using Prism.Mvvm;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Bespoke.Common;
using Bespoke.GestureClassifier.Framework;
using WiimoteLib;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class CombinationTrainingViewModel : BindableBase
    {
        public event EventHandler TrainedGesturesUpdated;

        public ObservableCollection<MultiGesture> Gestures { get; }

        public MultiGesture SelectedGesture
        {
            get
            {
                return mSelectedGesture;
            }
            set
            {
                if (SetProperty(ref mSelectedGesture, value))
                {
                    mEditCommand.RaiseCanExecuteChanged();
                    mDeleteCommand.RaiseCanExecuteChanged();
                    mTrainCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand AddCommand => mAddCommand;

        public ICommand EditCommand => mEditCommand;

        public ICommand DeleteCommand => mDeleteCommand;

        public ICommand TrainCommand => mTrainCommand;

        public IInteractionRequest AddCombinationGestureRequest => mAddCombinationGestureRequest;

        public IInteractionRequest TrainCombinationGestureRequest => mTrainCombinationGestureRequest;

        private IMessageBoxService MessageBoxService { get; }

        public CombinationTrainingViewModel(TrainedGestureSet trainedGestureSet, Wiimote leftHandWiimote, Wiimote rightHandWiimote, IMessageBoxService messageBoxService)
        {
            mTrainedGestureSet = trainedGestureSet;
            Gestures = new ObservableCollection<MultiGesture>(trainedGestureSet.MultiGestures);
            mLeftHandWiimote = leftHandWiimote;
            mRightHandWiimote = rightHandWiimote;

            mAddCommand = new DelegateCommand(OnAddCommand);
            mEditCommand = new DelegateCommand(OnEditCommand, () => SelectedGesture != null);
            mDeleteCommand = new DelegateCommand(OnDeleteCommand, () => SelectedGesture != null);
            mTrainCommand = new DelegateCommand(OnTrainCommand, () => SelectedGesture != null);

            mAddCombinationGestureRequest = new InteractionRequest<AddCombinationGestureViewModel>();
            mTrainCombinationGestureRequest = new InteractionRequest<TrainCombinationGestureViewModel>();
        }

        private void OnAddCommand()
        {
            mAddCombinationGestureRequest.Raise(new AddCombinationGestureViewModel(mTrainedGestureSet.TrainedGestureCollections[PlayerIndex.One], mTrainedGestureSet.TrainedGestureCollections[PlayerIndex.Two])
            {
                Title = "Add Combination Gesture"
            }, (viewModel) =>
            {
                if (viewModel.Confirmed)
                {
                    MultiGesture multiGesture = new MultiGesture(viewModel.GestureName);
                    if (Gestures.Contains(multiGesture))
                    {
                        MessageBoxService.Show($"The gesture name '{multiGesture.GestureName}' already exists", "Add Gesture Failed");
                    }
                    else
                    {
                        string leftHandGestureName = string.Empty;
                        if (viewModel.SelectedLeftHandTrainedGesture != null)
                        {
                            multiGesture.TrainedGestures.Add(PlayerIndex.One, viewModel.SelectedLeftHandTrainedGesture);
                            leftHandGestureName = viewModel.SelectedLeftHandTrainedGesture.GestureName;
                        }

                        string rightHandGestureName = string.Empty;
                        if (viewModel.SelectedRightHandTrainedGesture != null)
                        {
                            multiGesture.TrainedGestures.Add(PlayerIndex.Two, viewModel.SelectedRightHandTrainedGesture);
                            rightHandGestureName = viewModel.SelectedRightHandTrainedGesture.GestureName;
                        }

                        mTrainedGestureSet.AddMultiGesture(multiGesture);
                        Gestures.Add(multiGesture);
                        SelectedGesture = multiGesture;
                        OnTrainedGesturesUpdated();
                    }
                }
            });
        }

        private void OnEditCommand()
        {
            MultiGesture selectedGesture = SelectedGesture;

            mAddCombinationGestureRequest.Raise(new AddCombinationGestureViewModel(mTrainedGestureSet.TrainedGestureCollections[PlayerIndex.One], mTrainedGestureSet.TrainedGestureCollections[PlayerIndex.Two])
            {
                Title = "Rename Combination Gesture",
                GestureName = selectedGesture.GestureName,
                SelectedLeftHandTrainedGesture = selectedGesture.TrainedGestures[PlayerIndex.One],
                SelectedRightHandTrainedGesture = selectedGesture.TrainedGestures[PlayerIndex.Two]
            }, (viewModel) =>
            {
                if (viewModel.Confirmed && (viewModel.GestureName != selectedGesture.GestureName))
                {
                    var existingGestureQuery = from multiGesture in Gestures
                                               where multiGesture.GestureName == viewModel.GestureName
                                               select multiGesture;

                    if (existingGestureQuery.Count() == 0)
                    {
                        mTrainedGestureSet.RemoveMultiGesture(selectedGesture);
                        Gestures.Remove(selectedGesture);

                        MultiGesture multiGesture = new MultiGesture(viewModel.GestureName);
                        string leftHandGestureName = string.Empty;
                        if (viewModel.SelectedLeftHandTrainedGesture != null)
                        {
                            multiGesture.TrainedGestures.Add(PlayerIndex.One, viewModel.SelectedLeftHandTrainedGesture);
                            leftHandGestureName = viewModel.SelectedLeftHandTrainedGesture.GestureName;
                        }

                        string rightHandGestureName = string.Empty;
                        if (viewModel.SelectedRightHandTrainedGesture != null)
                        {
                            multiGesture.TrainedGestures.Add(PlayerIndex.Two, viewModel.SelectedRightHandTrainedGesture);
                            rightHandGestureName = viewModel.SelectedRightHandTrainedGesture.GestureName;
                        }

                        mTrainedGestureSet.AddMultiGesture(multiGesture);
                        Gestures.Add(multiGesture);
                        SelectedGesture = multiGesture;
                        OnTrainedGesturesUpdated();
                    }
                    else
                    {
                        MessageBoxService.Show($"The gesture name '{viewModel.GestureName}' already exists", "Edit Gesture Failed");
                    }
                }
            });
        }

        private void OnDeleteCommand()
        {
            MultiGesture selectedGesture = SelectedGesture;
            if (selectedGesture != null)
            {
                mTrainedGestureSet.RemoveMultiGesture(selectedGesture);
                Gestures.Remove(SelectedGesture);
                OnTrainedGesturesUpdated();
            }
        }

        private void OnTrainCommand()
        {
            using (var viewModel = new TrainCombinationGestureViewModel(SelectedGesture, mLeftHandWiimote, mRightHandWiimote) { Title = "Train Combination Gesture" })
            {
                viewModel.TrainedGesturesUpdated += (sender, e) => OnTrainedGesturesUpdated();
                mTrainCombinationGestureRequest.Raise(viewModel);
            }
        }

        private void OnTrainedGesturesUpdated()
        {
            EventHandler handler = TrainedGesturesUpdated;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private DelegateCommand mAddCommand;
        private DelegateCommand mEditCommand;
        private DelegateCommand mDeleteCommand;
        private DelegateCommand mTrainCommand;

        private InteractionRequest<AddCombinationGestureViewModel> mAddCombinationGestureRequest;
        private InteractionRequest<TrainCombinationGestureViewModel> mTrainCombinationGestureRequest;

        private Wiimote mLeftHandWiimote;
        private Wiimote mRightHandWiimote;

        private TrainedGestureSet mTrainedGestureSet;
        private MultiGesture mSelectedGesture;
    }
}
