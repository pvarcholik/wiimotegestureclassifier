﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using System.Collections.Generic;
using Bespoke.Common;
using Bespoke.GestureClassifier.Framework;
using WiimoteLib;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class TrainCombinationGestureViewModel : BindableBase, INotification, IInteractionRequestAware, IDisposable
    {
        public event EventHandler TrainedGesturesUpdated;

        public string Title { get; set; }

        public object Content { get; set; }

        public MultiGesture Gesture { get; set; }

        public Action FinishInteraction { get; set; }

        public INotification Notification { get; set; }

        public string LeftHandGestureName { get; }

        public string RightHandGestureName { get; }

        public int LeftHandSampleCount => (Gesture != null ? Gesture.TrainedGestures[PlayerIndex.One].Samples.Count : 0);

        public int RightHandSampleCount => (Gesture != null ? Gesture.TrainedGestures[PlayerIndex.Two].Samples.Count : 0);

        public bool IsSampleAvailable => mCurrentSample != null;

        public string CommitSampleText => CommitSampleStrings[IsSampleAvailable];

        public bool CollectingSample
        {
            get
            {
                return mCollectingSample;
            }
            set
            {
                if (SetProperty(ref mCollectingSample, value))
                {
                    OnPropertyChanged(nameof(SampleHelpText));
                    OnPropertyChanged(nameof(CommitSampleText));
                    mCommitSampleCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string SampleHelpText => SampleHelpStrings[CollectingSample];

        public ICommand DeleteLastSampleCommand => mDeleteLastSampleCommand;

        public ICommand CommitSampleCommand => mCommitSampleCommand;

        public ICommand CloseCommand { get; }

        public TrainCombinationGestureViewModel(MultiGesture gesture, Wiimote leftHandWiimote, Wiimote rightHandWiimote)
        {
            Assert.ParamIsNotNull(gesture);

            Gesture = gesture;
            mLeftHandWiimote = leftHandWiimote;
            mRightHandWiimote = rightHandWiimote;
            mRightHandWiimote.WiimoteChanged += Wiimote_WiimoteChanged;
            LeftHandGestureName = gesture.TrainedGestures[PlayerIndex.One].GestureName;
            RightHandGestureName = gesture.TrainedGestures[PlayerIndex.Two].GestureName;

            mCommitSampleCommand = new DelegateCommand(OnCommitSampleCommand, () => Gesture != null && IsSampleAvailable);
            mDeleteLastSampleCommand = new DelegateCommand(OnDeleteLastSampleCommand, () => Gesture != null && (LeftHandSampleCount > 0 || RightHandSampleCount > 0));
            CloseCommand = new DelegateCommand(() => OnFinishInteraction());
        }

        private void OnCommitSampleCommand()
        {
            Gesture.TrainedGestures[PlayerIndex.One].AddSample(mCurrentSample.Item1);
            Gesture.TrainedGestures[PlayerIndex.Two].AddSample(mCurrentSample.Item2);
            mCurrentSample = null;
            OnPropertyChanged(nameof(CommitSampleText));
            mCommitSampleCommand.RaiseCanExecuteChanged();
            mDeleteLastSampleCommand.RaiseCanExecuteChanged();
            OnPropertyChanged(nameof(LeftHandSampleCount));
            OnPropertyChanged(nameof(RightHandSampleCount));
            OnTrainedGesturesUpdated();
        }

        private void OnDeleteLastSampleCommand()
        {
            Assert.IsTrue("Gesture.SampleCount", (LeftHandSampleCount > 0 || RightHandSampleCount > 0));

            TrainedGesture leftHandTrainedGesture = Gesture.TrainedGestures[PlayerIndex.One];
            if (leftHandTrainedGesture.Samples.Count > 0)
            {
                leftHandTrainedGesture.RemoveSample(leftHandTrainedGesture.Samples.Last());
                OnPropertyChanged(nameof(LeftHandSampleCount));
            }

            TrainedGesture rightHandTrainedGesture = Gesture.TrainedGestures[PlayerIndex.Two];
            if (rightHandTrainedGesture.Samples.Count > 0)
            {
                rightHandTrainedGesture.RemoveSample(rightHandTrainedGesture.Samples.Last());
                OnPropertyChanged(nameof(RightHandSampleCount));
            }

            mDeleteLastSampleCommand.RaiseCanExecuteChanged();            
            OnTrainedGesturesUpdated();
        }

        private void Wiimote_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
        {
            WiimoteState rightHandCurrentWiimoteState = mRightHandWiimote.WiimoteState;
            WiimoteState leftHandCurrentWiimoteState = mLeftHandWiimote.WiimoteState;

            mRightHandLastWiimoteButtonState = mRightHandCurrentWiimoteButtonState;
            mRightHandCurrentWiimoteButtonState = rightHandCurrentWiimoteState.ButtonState;

            bool aButtonPressedThisFrame = ((mRightHandCurrentWiimoteButtonState.A) && (mRightHandLastWiimoteButtonState.A == false));
            if (mCommitSampleCommand.CanExecute() && aButtonPressedThisFrame)
            {
                Application.Current.Dispatcher.Invoke(mCommitSampleCommand.Execute);
                return;
            }

            bool bButtonPressedThisFrame = ((mRightHandCurrentWiimoteButtonState.B) && (mRightHandLastWiimoteButtonState.B == false));
            if (bButtonPressedThisFrame)
            {
                // Start collecting a new set of points.
                mLeftHandCurrentWiimoteSamplePoints = new WiimotePointCollection();
                mRightHandCurrentWiimoteSamplePoints = new WiimotePointCollection();
                Application.Current.Dispatcher.Invoke(() => CollectingSample = true);
                return;
            }

            bool bButtonReleaseThisFrame = ((mRightHandCurrentWiimoteButtonState.B == false) && (mRightHandLastWiimoteButtonState.B));
            if (bButtonReleaseThisFrame && (mRightHandCurrentWiimoteSamplePoints.Count > 1) && (mLeftHandCurrentWiimoteSamplePoints.Count > 1))
            {
                // Create a gesture sample out of the collected points.
                mCurrentSample = new Tuple<Gesture, Gesture>(new Gesture(mLeftHandCurrentWiimoteSamplePoints), new Gesture(mRightHandCurrentWiimoteSamplePoints));
                Application.Current.Dispatcher.Invoke(() => CollectingSample = false);
                return;
            }

            bool bButtonHeldDown = ((mRightHandCurrentWiimoteButtonState.B) && (mRightHandLastWiimoteButtonState.B));
            if (bButtonHeldDown)
            {
                DateTime timestamp = DateTime.Now;
                if (mRightHandCurrentWiimoteSamplePoints.ContainsKey(timestamp) == false)
                {
                    mRightHandCurrentWiimoteSamplePoints.Add(new WiimotePoint(rightHandCurrentWiimoteState, timestamp));
                }
                if (mLeftHandCurrentWiimoteSamplePoints.ContainsKey(timestamp) == false)
                {
                    mLeftHandCurrentWiimoteSamplePoints.Add(new WiimotePoint(leftHandCurrentWiimoteState, timestamp));
                }
            }
        }

        private void OnFinishInteraction()
        {
            Action handler = FinishInteraction;
            if (handler != null)
            {
                handler();
            }
        }

        private void OnTrainedGesturesUpdated()
        {
            EventHandler handler = TrainedGesturesUpdated;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public void Dispose()
        {
            mRightHandWiimote.WiimoteChanged -= Wiimote_WiimoteChanged;
        }

        private static readonly Dictionary<bool, string> CommitSampleStrings = new Dictionary<bool, string>() { { false, "  -- No Sample --  " }, { true, "  Commit Sample  " } };
        private static readonly Dictionary<bool, string> SampleHelpStrings = new Dictionary<bool, string>() { { false, "Press B to Begin Gesture" }, { true, "Release B to End Gesture" } };

        private Wiimote mLeftHandWiimote;
        private Wiimote mRightHandWiimote;
        private Tuple<Gesture, Gesture> mCurrentSample;
        private bool mCollectingSample;
        private ButtonState mRightHandCurrentWiimoteButtonState;
        private ButtonState mRightHandLastWiimoteButtonState;
        private WiimotePointCollection mLeftHandCurrentWiimoteSamplePoints;
        private WiimotePointCollection mRightHandCurrentWiimoteSamplePoints;

        private DelegateCommand mCommitSampleCommand;
        private DelegateCommand mDeleteLastSampleCommand;
    }
}
