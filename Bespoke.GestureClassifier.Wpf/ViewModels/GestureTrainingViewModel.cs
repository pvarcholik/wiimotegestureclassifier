﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Bespoke.GestureClassifier.Framework;
using Bespoke.Common;
using WiimoteLib;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class GestureTrainingViewModel : BindableBase, IHandleWiimoteEventsViewModel
    {
        public event EventHandler TrainedGesturesUpdated;

        public ObservableCollection<TrainedGesture> Gestures { get; }

        public TrainedGesture SelectedGesture
        {
            get
            {
                return mSelectedGesture;
            }
            set
            {
                if (SetProperty(ref mSelectedGesture, value))
                {
                    OnPropertyChanged(nameof(SampleCount));
                    mRenameTrainedGestureCommand.RaiseCanExecuteChanged();
                    mDeleteTrainedGestureCommand.RaiseCanExecuteChanged();
                    mSelectNextTrainedGestureCommand.RaiseCanExecuteChanged();
                    mSelectPreviousTrainedGestureCommand.RaiseCanExecuteChanged();
                    mCommitSampleCommand.RaiseCanExecuteChanged();
                    mDeleteLastSampleCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int SampleCount => (SelectedGesture != null ? SelectedGesture.Samples.Count : 0);

        public bool IsSampleAvailable => mCurrentSample != null;

        public string CommitSampleText => CommitSampleStrings[IsSampleAvailable];

        public bool CollectingSample
        {
            get
            {
                return mCollectingSample;
            }
            set
            {
                if (SetProperty(ref mCollectingSample, value))
                {
                    OnPropertyChanged(nameof(SampleHelpText));
                    OnPropertyChanged(nameof(CommitSampleText));
                    mCommitSampleCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string SampleHelpText => SampleHelpStrings[CollectingSample];

        public ICommand AddTrainedGestureCommand => mAddTrainedGestureCommand;

        public ICommand RenameTrainedGestureCommand => mRenameTrainedGestureCommand;

        public ICommand DeleteTrainedGestureCommand => mDeleteTrainedGestureCommand;

        public ICommand SelectNextTrainedGestureCommand => mSelectNextTrainedGestureCommand;

        public ICommand SelectPreviousTrainedGestureCommand => mSelectPreviousTrainedGestureCommand;

        public ICommand DeleteLastSampleCommand => mDeleteLastSampleCommand;

        public ICommand CommitSampleCommand => mCommitSampleCommand;

        public IInteractionRequest AddGestureRequest => mAddGestureRequest;

        public bool HandleWiimoteEvents
        {
            get
            {
                return mHandleWiimoteEvents;
            }
            set
            {
                if (SetProperty(ref mHandleWiimoteEvents, value) && Wiimote != null)
                {
                    if (mHandleWiimoteEvents)
                    {
                        Wiimote.WiimoteChanged += Wiimote_WiimoteChanged;
                    }
                    else
                    {
                        Wiimote.WiimoteChanged -= Wiimote_WiimoteChanged;
                    }
                }
            }
        }

        private Wiimote Wiimote { get; }

        private IMessageBoxService MessageBoxService { get; }

        public GestureTrainingViewModel(TrainedGestureSet trainedGestureSet, PlayerIndex playerIndex, Wiimote wiimote, IMessageBoxService messageBoxService)
        {
            mTrainedGestureSet = trainedGestureSet;
            mPlayerIndex = playerIndex;
            Gestures = mTrainedGestureSet.TrainedGestureCollections[playerIndex];
            Wiimote = wiimote;
            HandleWiimoteEvents = false;
            MessageBoxService = messageBoxService;

            mAddTrainedGestureCommand = new DelegateCommand(OnAddTrainedGestureCommand);
            mRenameTrainedGestureCommand = new DelegateCommand(OnRenameTrainedGestureCommand, () => SelectedGesture != null);
            mDeleteTrainedGestureCommand = new DelegateCommand(OnDeleteTrainedGestureCommand, () => SelectedGesture != null);
            mSelectNextTrainedGestureCommand = new DelegateCommand(OnSelectNextTrainedGestureCommand, () => Gestures.Count > 0 && (SelectedGesture == null || SelectedGesture != Gestures.Last()));
            mSelectPreviousTrainedGestureCommand = new DelegateCommand(OnSelectPreviousTrainedGestureCommand, () => Gestures.Count > 0 && (SelectedGesture == null || SelectedGesture != Gestures.First()));
            mCommitSampleCommand = new DelegateCommand(OnCommitSampleCommand, () => SelectedGesture != null && IsSampleAvailable);
            mDeleteLastSampleCommand = new DelegateCommand(OnDeleteLastSampleCommand, () => SelectedGesture != null && SampleCount > 0);

            mAddGestureRequest = new InteractionRequest<AddGestureViewModel>();
        }

        private void OnAddTrainedGestureCommand()
        {
            mAddGestureRequest.Raise(new AddGestureViewModel() { Title = "Add Gesture" }, (viewModel) =>
            {
                if (viewModel.Confirmed)
                {
                    TrainedGesture trainedGesture = new TrainedGesture(viewModel.GestureName, mPlayerIndex);

                    if (Gestures.Contains(trainedGesture))
                    {
                        MessageBoxService.Show($"The gesture name '{trainedGesture.GestureName}' already exists", "Add Gesture Failed");
                    }
                    else
                    {
                        Gestures.Add(trainedGesture);
                        SelectedGesture = trainedGesture;
                        OnTrainedGesturesUpdated();
                    }
                }
            });
        }

        private void OnRenameTrainedGestureCommand()
        {
            TrainedGesture selectedGesture = SelectedGesture;

            mAddGestureRequest.Raise(new AddGestureViewModel() { Title = "Rename Gesture", GestureName = selectedGesture.GestureName }, (viewModel) =>
            {
                if (viewModel.Confirmed && (viewModel.GestureName != selectedGesture.GestureName))
                {
                    var existingGestureQuery = from trainedGesture in Gestures
                                               where trainedGesture.GestureName == viewModel.GestureName
                                               select trainedGesture;

                    if (existingGestureQuery.Count() == 0)
                    {
                        Gestures.Remove(selectedGesture);
                        selectedGesture.GestureName = viewModel.GestureName;
                        Gestures.Add(selectedGesture);
                        SelectedGesture = selectedGesture;
                        OnTrainedGesturesUpdated();
                    }
                    else
                    {
                        MessageBoxService.Show($"The gesture name '{viewModel.GestureName}' already exists", "Rename Gesture Failed");
                    }
                }
            });
        }

        private void OnDeleteTrainedGestureCommand()
        {
            TrainedGesture selectedGesture = SelectedGesture;
            if (selectedGesture != null)
            {
                Gestures.Remove(selectedGesture);
                mCurrentSample = null;
                OnPropertyChanged(nameof(CommitSampleText));
                mCommitSampleCommand.RaiseCanExecuteChanged();
                mDeleteLastSampleCommand.RaiseCanExecuteChanged();
                OnPropertyChanged(nameof(SampleCount));
                OnTrainedGesturesUpdated();
            }
        }

        private void OnSelectNextTrainedGestureCommand()
        {
            if (SelectedGesture == null)
            {
                SelectedGesture = Gestures.FirstOrDefault();
            }
            else
            {
                int selectedIndex = Gestures.IndexOf(SelectedGesture);
                selectedIndex++;
                if (selectedIndex < Gestures.Count)
                {
                    SelectedGesture = Gestures[selectedIndex];
                }
            }
        }

        private void OnSelectPreviousTrainedGestureCommand()
        {
            if (SelectedGesture == null)
            {
                SelectedGesture = Gestures.FirstOrDefault();
            }
            else
            {
                int selectedIndex = Gestures.IndexOf(SelectedGesture);
                selectedIndex--;
                if (selectedIndex >= 0)
                {
                    SelectedGesture = Gestures[selectedIndex];
                }
            }
        }

        private void OnCommitSampleCommand()
        {
            SelectedGesture.AddSample(mCurrentSample);
            mCurrentSample = null;
            OnPropertyChanged(nameof(CommitSampleText));
            mCommitSampleCommand.RaiseCanExecuteChanged();
            mDeleteLastSampleCommand.RaiseCanExecuteChanged();
            OnPropertyChanged(nameof(SampleCount));
            OnTrainedGesturesUpdated();
        }

        private void OnDeleteLastSampleCommand()
        {
            Assert.IsTrue("SelectedGesture.SampleCount", SampleCount > 0);

            SelectedGesture.RemoveSample(SelectedGesture.Samples.Last());
            mDeleteLastSampleCommand.RaiseCanExecuteChanged();
            OnPropertyChanged(nameof(SampleCount));
            OnTrainedGesturesUpdated();
        }

        private void Wiimote_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
        {
            WiimoteState currentWiimoteState = Wiimote.WiimoteState;
            mLastWiimoteButtonState = mCurrentWiimoteButtonState;
            mCurrentWiimoteButtonState = currentWiimoteState.ButtonState;

            bool downButtonPressedThisFrame = ((mCurrentWiimoteButtonState.Down) && (mLastWiimoteButtonState.Down == false));
            if (mSelectNextTrainedGestureCommand.CanExecute() && downButtonPressedThisFrame)
            {
                Application.Current.Dispatcher.Invoke(mSelectNextTrainedGestureCommand.Execute);
                return;
            }

            bool upButtonPressedThisFrame = ((mCurrentWiimoteButtonState.Up) && (mLastWiimoteButtonState.Up == false));
            if (mSelectPreviousTrainedGestureCommand.CanExecute() && upButtonPressedThisFrame)
            {
                Application.Current.Dispatcher.Invoke(mSelectPreviousTrainedGestureCommand.Execute);
                return;
            }

            bool aButtonPressedThisFrame = ((mCurrentWiimoteButtonState.A) && (mLastWiimoteButtonState.A == false));
            if (mCommitSampleCommand.CanExecute() && aButtonPressedThisFrame)
            {
                Application.Current.Dispatcher.Invoke(mCommitSampleCommand.Execute);
                return;
            }

            if (SelectedGesture != null)
            {
                // Collect sample
                bool bButtonPressedThisFrame = ((mCurrentWiimoteButtonState.B) && (mLastWiimoteButtonState.B == false));
                if (bButtonPressedThisFrame)
                {
                    // Start collecting a new set of points.
                    mCurrentWiimoteSamplePoints = new WiimotePointCollection();
                    Application.Current.Dispatcher.Invoke(() => CollectingSample = true);
                    return;
                }

                bool bButtonReleaseThisFrame = ((mCurrentWiimoteButtonState.B == false) && (mLastWiimoteButtonState.B));
                if (bButtonReleaseThisFrame && mCurrentWiimoteSamplePoints.Count > 1)
                {
                    // Create a gesture sample out of the collected points.
                    mCurrentSample = new Gesture(mCurrentWiimoteSamplePoints);
                    Application.Current.Dispatcher.Invoke(() => CollectingSample = false);
                    return;
                }

                bool bButtonHeldDown = ((mCurrentWiimoteButtonState.B) && (mLastWiimoteButtonState.B));
                if (bButtonHeldDown)
                {
                    DateTime timestamp = DateTime.Now;
                    if (mCurrentWiimoteSamplePoints.ContainsKey(timestamp) == false)
                    {
                        mCurrentWiimoteSamplePoints.Add(new WiimotePoint(currentWiimoteState, timestamp));
                    }
                }
            }
        }

        private void OnTrainedGesturesUpdated()
        {
            EventHandler handler = TrainedGesturesUpdated;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private static readonly Dictionary<bool, string> CommitSampleStrings = new Dictionary<bool, string>() { { false, "  -- No Sample --  " }, { true, "  Commit Sample  " } };
        private static readonly Dictionary<bool, string> SampleHelpStrings = new Dictionary<bool, string>() { { false, "Press B to Begin Gesture" }, { true, "Release B to End Gesture" } };

        private DelegateCommand mAddTrainedGestureCommand;
        private DelegateCommand mRenameTrainedGestureCommand;
        private DelegateCommand mDeleteTrainedGestureCommand;
        private DelegateCommand mSelectNextTrainedGestureCommand;
        private DelegateCommand mSelectPreviousTrainedGestureCommand;
        private DelegateCommand mCommitSampleCommand;
        private DelegateCommand mDeleteLastSampleCommand;

        private InteractionRequest<AddGestureViewModel> mAddGestureRequest;

        private TrainedGestureSet mTrainedGestureSet;
        private PlayerIndex mPlayerIndex;
        private TrainedGesture mSelectedGesture;
        private Gesture mCurrentSample;
        private bool mCollectingSample;

        private ButtonState mCurrentWiimoteButtonState;
        private ButtonState mLastWiimoteButtonState;
        private WiimotePointCollection mCurrentWiimoteSamplePoints;
        private bool mHandleWiimoteEvents;
    }
}
